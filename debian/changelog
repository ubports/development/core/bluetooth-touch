bluetooth-touch (0.4.0) UNRELEASED; urgency=medium

  * Port to focal
  * Update homepage and VCS URLs
  * Update to dh version 12
  * Clean up rules
  * Switch to source format 3.0 (native)

 -- Guido Berhoerster <guido+gitlab.com@berhoerster.name>  Wed, 27 Apr 2022 14:44:06 +0200

bluetooth-touch (0.3.3+16.04.20160201-0ubuntu1) xenial; urgency=medium

  * Correctly check for upstart job configuration file

 -- Simon Fels <simon.busch@canonical.com>  Mon, 01 Feb 2016 11:17:45 +0000

bluetooth-touch (0.3.3+16.04.20160118-0ubuntu1) xenial; urgency=medium

  [ Simon Fels ]
  * Add citrain support
  * Add citrain support removed: debian/source/ debian/source/format
    added: .bzr-builddeb/ .bzr-builddeb/default.conf
  * Add option to override upstart job for Bluetooth initialization
    added: debian/bluetooth-touch-android.conf
  * Drop unwanted bzr-builddeb configuration removed: debian/bzr-
    builddeb.conf

 -- Thomas Voß <ci-train-bot@canonical.com>  Mon, 18 Jan 2016 15:04:19 +0000

bluetooth-touch (0.3.2) wily; urgency=medium

  * Update the systemd job to check that $SCRIPT is a regular file before
    attempting to run it. (LP: #1437671)

 -- Christopher James Halse Rogers <raof@ubuntu.com>  Mon, 19 Oct 2015 10:17:56 +1100

bluetooth-touch (0.3.1) vivid; urgency=medium

  * Adding missing +x to the scripts, otherwise every bluetooth touch
    job will fail

 -- Ricardo Salveti de Araujo <ricardo.salveti@canonical.com>  Fri, 16 Jan 2015 18:34:16 -0200

bluetooth-touch (0.3.0) vivid; urgency=medium

  * Separate upstart jobs content in independent scripts installed in
    usr/share/bluetooth-touch.
  * Change upstart jobs to point at those scripts.
  * Add a systemd unit running only on Touch executing the right script
    depending on the platform.
  * Add some COPYING file to precise license info of those scripts
  * Bump Standards-Version.

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 15 Jan 2015 12:14:18 +0100

bluetooth-touch (0.2.3) utopic; urgency=medium

  * Use dpkg-buildflags when compiling, ensuring all hardening flags are used
    and debug symbols are available.

 -- Steve Langasek <steve.langasek@ubuntu.com>  Thu, 24 Jul 2014 11:43:47 -0700

bluetooth-touch (0.2.2) trusty; urgency=medium

  * add upstart job for flo device

 -- Oliver Grawert <ogra@ubuntu.com>  Wed, 26 Feb 2014 20:20:00 +0100

bluetooth-touch (0.2.1) saucy; urgency=low

  * 40-brcm_patchram_plus-disable: Disable brcm_patchram_plus in the Android
    container, so as not to interfere with our own copy that runs in the rootfs.
    (LP: #1236247)

 -- Mathieu Trudel-Lapierre <mathieu-tl@ubuntu.com>  Fri, 11 Oct 2013 10:26:18 -0400

bluetooth-touch (0.2.0) saucy; urgency=low

  * Rename Source and Binary to bluetooth-touch to be more generic.
  * Add/revise upstart jobs for maguro, mako and grouper.
  * debian/source/format: we're using source format 3.0 (native).

 -- Mathieu Trudel-Lapierre <mathieu-tl@ubuntu.com>  Tue, 13 Aug 2013 16:42:48 -0400
